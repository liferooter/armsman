<!--
SPDX-FileCopyrightText: 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->

<div align="center">
<img src="assets/armsman.svg" width="150"/>
<h1> Armsman</h1>
Automatize your development tasks
</div>

<br/>

# What is it?

Armsman is an automation tool for development tasks. Write one configuration file and run any of defined tasks from CI/CD, as build scripts, as IDE tasks, as pre-commit check and so on.

# General overview

To use Armsman, write a configuration file for the project. Configuration file should be placed in the root of the project and named `armsman.yml`.

Configuration should define some stages. A stage can contain some tasks. A task is a script written in any preferred language (`bash` by default). Then stages can be run in some sequence. E.g. stages `check`, `build` and `test` can be run using following command:

```bash
armsman run-stages check build test
# or
armsman -s check build test
```

Also particular tasks from some stages can be run. E.g. you can run task `licensing` from stage `check` using following command:

```bash
armsman run-task check:licensing
# or
armsman -t check:licensing
```

Also some sequences of stages can be saved for future use as workflows and then run without specifying the sequence of targets. E.g. workflow with name `ci` can be run using following command:

```bash
armsman run-workflow ci
# or
armsman -w ci
```


# Configuration format

Configuration file is YAML document located in the root of the project and named `armsman.yaml`. Configuration may have following structure:

## Root fields

### `env`

Environment variables to override for tasks.

**Type:** mapping from string to string

**Required:** no

**Example:**

```yaml
env:
  RUST_BACKTRACE: 1
  LD_LIBRARY_PATH: ./external_libs/
```

### `import-env`

A script that outputs environment overrides in a form of `env` command.

**Type:** [script](#script)

**Required:** no

**Example:**

```yaml
import-env: nix-shell -p nodejs-18_x --run env
```

### `stages`

A list of stages that can be run.

**Type:** mapping from string to [stage](#stage)

**Required:** yes

**Example:**

```yaml
stages:
  check:
    cargo: cargo check
    clippy: cargo clippy -- -D warnings
    format: cargo fmt --check

  test:
    cargo: cargo test

  build:
    ensure-cargo: >
      which cargo 2> /dev/null ||
      curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal

    cargo: cargo build

  run:
    cargo: cargo run
```

### `workflows`

A list of workflows.

Each workflow is a list of stage names.

**Type:** mapping from string to list of string

**Required:** yes

**Example:**

```yaml
workflows:
  ci:
  - check
  - build
  - test
  - release
  pre-commit:
  - check
  - test
  run:
  - build
  - run
```

## Stage

A list of tasks belonging to the stage.

**Type:** sequence of [tasks](#task)

**Required:** yes

## Task

A [script](#script) or a [remote preset](#remote-preset).

## Script

A script to run. Can be either in short form or in full form.

### Short form

Script in short form is just a shell script code. `bash` is used as a runner for script defined in short form.

### Full form

Script in a short has following fields:

| Field    | Type                            | Required               | Description                 |
| -------- | ------------------------------- | ---------------------- | --------------------------- |
| `code`   | [script source](#script-source) | yes                    | A source of the script code |
| `runner` | string                          | no, defaults to `bash` | A binary to run script with |
| `env`    | mapping from string to string   | no                     | Environment overrides       |

### Script source

Script source is either raw code string, local file (has the only field `file` with file path), remote file (has the only field `url` with script URL) or file from Git repository (has fields `repo` for Git repository URL, `file` for file path in the repository and optional `rev` for revision to checkout).

## Remote preset

Remote preset is a task fetched from [remote presets repository](#remote-presets-repository).
There're two types of remote presets: Git remote presets and URL remote presets.

### Remote presets repository

Remote presets repository is a set of pre-defined scripts locate remotely as file `armsman-presets.yml` in Git repository or as raw text at some URL. Presets from Git repository can use script from the repository.

Remote presets repository is another YAML document with two fields: optinal `env` field contains environment overrides that is used for all presets and `presets` field is a mapping from preset name to its [script](#script).

### Git remote presets

Git remote presets have following fields: `repo` for Git repository URL, `preset` for name of preset to take from remote presets repository and optional `rev` for revision to checkout.

### URL remote presets

URL remote presets have following fields: `url` for remote presets repository URL and `preset` for name of preset to take from remote presets repository.

# Example

This is very abstract example of Armsman configuration:

```yaml
import-env: nix-shell -p reuse --run env
env:
  APP_ID: org.example.Application

stages:

  check:
    reuse: reuse lint
    validate-something:
      runner: python
      code: |
        import validatelib

        validatelib.validate('some_file.rs')
      env:
        PYTHON_PATH: ./modules/validatelib/

  build:
    meson:
      git: https://git.example.org/someone/armsman-scripts
      rev: v1.0
      preset: build-with-meson
    flatpak:
      code:
        url: https://example.org/scripts/build-flatpak.sh

  test:
    units:
      code:
        file: ./my-script.sh

    integration:
      code:
        git: https://git.example.org/someone/scripts
        file: scripts/integration-test.sh

  run:
    run: ./run.sh


workflows:
  ci:
  - check
  - test

  run:
  - build
  - run
```
