// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, path::PathBuf};

/// Parse context.
#[derive(Debug, Clone)]
pub struct Context {
    pub recursion_depth: u32,
    pub root: Option<PathBuf>,
    pub run_root: PathBuf,
    pub global_env: HashMap<String, String>,
}
