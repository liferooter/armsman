// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::error::Error;
use std::process::exit;

use config::project::{LoadProjectError, ProjectConfig};
use termion::{
    color::{self, Fg, Red},
    style::{self, Bold},
};

mod cli;
mod config;
mod context;
mod remote;
mod targets;

fn main() {
    let cfg = match ProjectConfig::load() {
        Ok(cfg) => Some(cfg),
        Err(LoadProjectError::NoRoot) => None,
        Err(err) => throw_error(err),
    };
    cli::run(cfg).unwrap_or_else(throw_error)
}

fn throw_error<T>(err: impl Error) -> T {
    eprintln!();
    eprintln!(
        "{}{}error:{}{} {}",
        Fg(Red),
        Bold,
        style::Reset,
        Fg(color::Reset),
        err
    );
    exit(1)
}
