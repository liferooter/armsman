// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use serde::Deserialize;

use super::task::ScriptConfig;

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct PresetsConfig {
    pub env: Option<HashMap<String, String>>,
    pub presets: HashMap<String, ScriptConfig>,
}
