// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, env::current_dir, fs, io, path::PathBuf};

use linked_hash_map::LinkedHashMap;
use serde::Deserialize;
use thiserror::Error;

use crate::{context::Context, targets::RunError};

use super::task::{ScriptConfig, ScriptLoadError, TaskConfig};

const CONF_FILE: &str = "armsman.yml";

#[derive(Clone, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct ProjectConfig {
    #[serde(rename = "import-env")]
    pub import_env: Option<ScriptConfig>,
    pub env: Option<HashMap<String, String>>,
    pub stages: LinkedHashMap<String, LinkedHashMap<String, TaskConfig>>,
    pub workflows: Option<LinkedHashMap<String, Vec<String>>>,
}

#[derive(Debug, Error)]
pub enum LoadProjectError {
    #[error("failed to find `armsman.yml` in current directory or its parents")]
    NoRoot,
    #[error("failed to find `armsman.yml` in current directory or its parents: {0}")]
    IOError(#[from] io::Error),

    #[error("invalid configuration: {0}")]
    DeserializeError(#[from] serde_yml::Error),

    #[error("failed to load environment import target: {0}")]
    ImportEnvLoadError(#[from] ScriptLoadError),
    #[error("environment import failed: {0}")]
    ImportEnvFailed(#[from] RunError),
    #[error("invalid environment format")]
    WrongEnvironmentFormat,
}

impl ProjectConfig {
    pub fn load() -> Result<(Self, Context), LoadProjectError> {
        let file = find_config()?.ok_or(LoadProjectError::NoRoot)?;
        let root = file.parent().unwrap().to_path_buf();

        let raw_yaml = fs::read_to_string(&file)?;
        let cfg: Self = serde_yml::from_str(&raw_yaml)?;
        let mut global_env = cfg.env.clone().unwrap_or_default();

        if let Some(script) = cfg.import_env.clone() {
            let task = script.into_runnable(
                String::new(),
                Context {
                    recursion_depth: 0,
                    root: Some(root.clone()),
                    run_root: root.clone(),
                    global_env: cfg.env.clone().unwrap_or_default(),
                },
            )?;

            let output = task.run_raw(true)?;
            let output = String::from_utf8(output.stdout)
                .map_err(|_| LoadProjectError::WrongEnvironmentFormat)?;
            let envvars: HashMap<String, String> = output
                .lines()
                .map(|line| {
                    let equal_sign = line.find('=')?;
                    Some((line[..equal_sign].to_owned(), line[equal_sign..].to_owned()))
                })
                .collect::<Option<_>>()
                .ok_or(LoadProjectError::WrongEnvironmentFormat)?;

            global_env = envvars.into_iter().chain(global_env).collect()
        }

        Ok((
            cfg,
            Context {
                recursion_depth: 0,
                run_root: root.clone(),
                root: Some(root),
                global_env,
            },
        ))
    }

    pub fn get_stage(&self, name: &str) -> Option<&LinkedHashMap<String, TaskConfig>> {
        self.stages.get(name)
    }

    pub fn get_workflow(&self, name: &str) -> Option<&Vec<String>> {
        self.workflows.as_ref()?.get(name)
    }
}

fn find_config() -> io::Result<Option<PathBuf>> {
    for parent in current_dir()?.ancestors() {
        let mut path = parent.to_path_buf();
        path.push(CONF_FILE);
        if path.exists() {
            return Ok(Some(path));
        }
    }

    Ok(None)
}
