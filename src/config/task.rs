// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{collections::HashMap, fs, io, path::PathBuf};

use reqwest::blocking::get;
use serde::Deserialize;
use thiserror::Error;

use crate::remote::{fetch_from_git, GitError};

type Url = String;

#[derive(Clone, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum TaskConfig {
    Script(ScriptConfig),
    Preset(LoadPresetConfig),
}

#[derive(Clone, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum ScriptConfig {
    ShortForm(String),
    FullForm {
        code: ScriptSource,
        runner: Option<String>,
        env: Option<HashMap<String, String>>,
    },
}

#[derive(Clone, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum ScriptSource {
    RawCode(String),
    File {
        file: PathBuf,
    },
    FetchUrl {
        url: Url,
    },
    FetchGit {
        repo: Url,
        rev: Option<String>,
        file: String,
    },
}

#[derive(Debug, Error)]
pub enum ScriptLoadError {
    #[error(transparent)]
    IO(#[from] io::Error),
    #[error(transparent)]
    Network(#[from] reqwest::Error),
    #[error(transparent)]
    Git(#[from] GitError),
}

impl ScriptSource {
    pub fn get_code(self) -> Result<String, ScriptLoadError> {
        match self {
            Self::RawCode(code) => Ok(code),
            Self::File { file } => Ok(fs::read_to_string(file)?),
            Self::FetchUrl { url } => Ok(get(url)?.text()?),
            Self::FetchGit { repo, rev, file } => {
                Ok(fetch_from_git(&repo, rev.as_deref(), file)?.1)
            }
        }
    }
}

#[derive(Clone, Deserialize)]
#[serde(untagged, deny_unknown_fields)]
pub enum LoadPresetConfig {
    GitPreset {
        repo: String,
        rev: Option<String>,
        preset: String,
    },
    UrlPreset {
        url: String,
        preset: String,
    },
}
