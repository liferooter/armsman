// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::{self, remove_dir_all},
    io,
    path::{Path, PathBuf},
};

use git2::{build::CheckoutBuilder, Repository};
use sha2::{Digest, Sha256};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum GitError {
    #[error("failed to get cache directory")]
    NoCacheDir,
    #[error("failed to checkout Git repository: {0}")]
    Git(#[from] git2::Error),
    #[error("failed to read from file: {0}")]
    ReadFile(io::Error),
    #[error("failed to delete Git metadata: {0}")]
    DeleteGitMetadata(io::Error),
}

pub fn git_cache_dir() -> Option<PathBuf> {
    let mut path = dirs::cache_dir()?;
    path.push("armsman");
    path.push("git");
    Some(path)
}

pub fn fetch_from_git(
    url: &str,
    rev: Option<&str>,
    file: impl AsRef<Path>,
) -> Result<(PathBuf, String), GitError> {
    // Get hash of url
    let mut hasher = Sha256::new();
    hasher.update(url);
    hasher.update(rev.unwrap_or_default());
    let hash = hex::encode(hasher.finalize());

    // Get path where to clone Git repo
    let mut path = git_cache_dir().ok_or(GitError::NoCacheDir)?;
    path.push(hash);

    if !path.exists() {
        // Clone repository
        let repo = Repository::clone_recurse(url, &path)?;

        // Checkout revision
        let rev = repo.revparse_single(rev.unwrap_or("refs/remotes/origin/HEAD"))?;
        repo.checkout_tree(&rev, Some(CheckoutBuilder::new().force()))?;

        // Remove Git metadata and commit tree
        let mut git_dir = path.clone();
        git_dir.push(".git");

        remove_dir_all(git_dir).map_err(GitError::DeleteGitMetadata)?;
    };

    let mut file_path = path.clone();
    file_path.push(file);

    Ok((
        path,
        fs::read_to_string(file_path).map_err(GitError::ReadFile)?,
    ))
}
