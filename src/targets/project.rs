// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::{config::task::TaskConfig, context::Context, ProjectConfig};

use super::{target::TargetType, task::TaskLoadError, RunError};

use thiserror::Error;

use TargetType::*;
use TaskRunError::*;

#[derive(Debug, Error)]
pub enum TaskRunError {
    #[error("{0} '{1}' is not found")]
    NotFound(TargetType, String),
    #[error("failed to load task '{task}': {error}")]
    LoadFailed { task: String, error: TaskLoadError },
    #[error("task '{task}' failed: {error}")]
    RunFailed { task: String, error: RunError },
}

impl ProjectConfig {
    pub fn run_stage(&self, stage: &str, ctx: Context) -> Result<(), TaskRunError> {
        let tasks = self
            .get_stage(stage)
            .ok_or_else(|| NotFound(Stage, stage.to_owned()))?;

        for (task_name, task) in tasks {
            let name = format!("{stage}:{task_name}");

            task.clone()
                .into_runnable(name.clone(), ctx.clone())
                .map_err(|err| LoadFailed {
                    task: name.clone(),
                    error: err,
                })?
                .run()
                .map_err(|err| RunFailed {
                    task: name,
                    error: err,
                })?;
        }

        Ok(())
    }

    pub fn run_task(&self, stage: &str, task: &str, ctx: Context) -> Result<(), TaskRunError> {
        let name = format!("{stage}:{task}");

        let tasks = self
            .get_stage(stage)
            .ok_or_else(|| NotFound(Stage, stage.to_owned()))?;

        let task: TaskConfig = tasks
            .get(task)
            .ok_or_else(|| NotFound(Task, task.to_owned()))?
            .clone();

        task.into_runnable(name.clone(), ctx)
            .map_err(|err| LoadFailed {
                task: name.clone(),
                error: err,
            })?
            .run()
            .map_err(|err| RunFailed {
                task: name,
                error: err,
            })?;

        Ok(())
    }

    pub fn run_workflow(&self, workflow: &str, ctx: Context) -> Result<(), TaskRunError> {
        let stages = self
            .get_workflow(workflow)
            .ok_or_else(|| NotFound(Workflow, workflow.to_owned()))?;

        for stage in stages {
            self.run_stage(stage, ctx.clone())?;
        }

        Ok(())
    }
}
