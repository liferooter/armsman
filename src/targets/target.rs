// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub enum TargetType {
    Task,
    Stage,
    Workflow,
}

impl Display for TargetType {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        f.write_str(match self {
            Self::Task => "task",
            Self::Stage => "stage",
            Self::Workflow => "workflow",
        })
    }
}
