// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod preset;
mod project;
mod run;
mod target;
mod task;

pub use project::TaskRunError;
pub use run::{RunError, Runnable, DEFAULT_RUNNER};
pub use target::TargetType;
