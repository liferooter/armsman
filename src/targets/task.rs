// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use thiserror::Error;

use crate::{
    config::task::{ScriptConfig, ScriptLoadError, TaskConfig},
    context::Context,
};

use super::{preset::PresetLoadError, Runnable, DEFAULT_RUNNER};

#[derive(Debug, Error)]
pub enum TaskLoadError {
    #[error("failed to load script: {0}")]
    Script(#[from] ScriptLoadError),
    #[error("failed to load remote preset: {0}")]
    Preset(#[from] PresetLoadError),
}

impl TaskConfig {
    pub fn into_runnable(self, name: String, ctx: Context) -> Result<Runnable, TaskLoadError> {
        match self {
            Self::Script(script) => Ok(script.into_runnable(name, ctx)?),
            Self::Preset(preset) => Ok(preset.load(name, ctx)?),
        }
    }
}

impl ScriptConfig {
    pub fn into_runnable(self, name: String, ctx: Context) -> Result<Runnable, ScriptLoadError> {
        match self {
            Self::ShortForm(code) => Ok(Runnable {
                name,
                code,
                runner: DEFAULT_RUNNER.to_owned(),
                env: HashMap::new(),
                ctx,
            }),
            Self::FullForm { code, runner, env } => Ok(Runnable {
                name,
                code: code.get_code()?,
                runner: runner.unwrap_or_else(|| DEFAULT_RUNNER.to_owned()),
                env: env.unwrap_or_default(),
                ctx,
            }),
        }
    }
}
