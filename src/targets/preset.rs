// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use reqwest::blocking::get;
use thiserror::Error;

use crate::{
    config::{
        preset::PresetsConfig,
        task::{LoadPresetConfig, ScriptLoadError},
    },
    context::Context,
    remote::{fetch_from_git, GitError},
};

use super::Runnable;

const RECURSION_LIMIT: u32 = 24;

#[derive(Debug, Error)]
pub enum PresetLoadError {
    #[error("preset '{0}' not found in remote presets")]
    NotFound(String),
    #[error(transparent)]
    NetworkError(#[from] reqwest::Error),
    #[error(transparent)]
    GitError(#[from] GitError),
    #[error(transparent)]
    DeserializeError(#[from] serde_yml::Error),
    #[error("failed to load preset script: {0}")]
    TaskLoadError(#[from] ScriptLoadError),
    #[error("maximum recursion depth exceeded")]
    RecursionLimit,
}

impl LoadPresetConfig {
    pub fn load(self, name: String, ctx: Context) -> Result<Runnable, PresetLoadError> {
        use PresetLoadError::*;

        if ctx.recursion_depth > RECURSION_LIMIT {
            return Err(RecursionLimit);
        }

        match self {
            Self::UrlPreset {
                url,
                preset: preset_name,
            } => {
                let preset: PresetsConfig = serde_yml::from_str(&get(url)?.text()?)?;
                let ctx = Context {
                    recursion_depth: ctx.recursion_depth + 1,
                    root: None,
                    global_env: ctx
                        .global_env
                        .into_iter()
                        .chain(preset.env.unwrap_or_default())
                        .collect(),
                    ..ctx
                };
                let script_cfg = preset
                    .presets
                    .get(&preset_name)
                    .ok_or(NotFound(preset_name))?
                    .clone();
                Ok(script_cfg.into_runnable(name, ctx)?)
            }
            Self::GitPreset {
                repo: git,
                rev,
                preset: preset_name,
            } => {
                let (root, preset) = fetch_from_git(&git, rev.as_deref(), "armsman-presets.yml")?;
                let preset: PresetsConfig = serde_yml::from_str(&preset)?;
                let ctx = Context {
                    recursion_depth: ctx.recursion_depth + 1,
                    root: Some(root),
                    global_env: ctx
                        .global_env
                        .into_iter()
                        .chain(preset.env.unwrap_or_default())
                        .collect(),
                    ..ctx
                };
                let script_cfg = preset
                    .presets
                    .get(&preset_name)
                    .ok_or(NotFound(preset_name))?
                    .clone();
                Ok(script_cfg.into_runnable(name, ctx)?)
            }
        }
    }
}
