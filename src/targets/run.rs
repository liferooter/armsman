// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    collections::HashMap,
    fs, io,
    process::{Command, Output, Stdio},
};

use termion::{
    color::{self, Color, Fg, White, Yellow},
    style::{self, Bold},
};
use thiserror::Error;

use crate::context::Context;

pub const DEFAULT_RUNNER: &str = "bash";

#[derive(Debug)]
pub struct Runnable {
    pub name: String,
    pub code: String,
    pub runner: String,
    pub env: HashMap<String, String>,
    pub ctx: Context,
}

#[derive(Error, Debug)]
pub enum RunError {
    #[error("failed to write script file: {0}")]
    ScriptFile(#[source] io::Error),
    #[error("failed to run target: {0}")]
    Runtime(#[from] io::Error),
    #[error("script finished with non-zero exit code")]
    Failed,
}

impl Runnable {
    pub fn run(self) -> Result<(), RunError> {
        self.log_running();

        let output = self.run_raw(false)?;

        if output.status.success() {
            self.log_finished();
            Ok(())
        } else {
            Err(RunError::Failed)
        }
    }

    pub fn run_raw(&self, capture_output: bool) -> Result<Output, RunError> {
        let Runnable {
            code,
            runner,
            env,
            ctx,
            ..
        } = self;

        let source_file = tempfile::Builder::new()
            .tempfile()
            .map_err(RunError::ScriptFile)?;

        fs::write(&source_file, code).map_err(RunError::ScriptFile)?;
        Command::new(runner)
            .arg(source_file.path())
            .current_dir(&ctx.run_root)
            .envs(env)
            .envs(&ctx.global_env)
            .stdout(if capture_output {
                Stdio::piped()
            } else {
                Stdio::inherit()
            })
            .output()
            .map_err(Into::into)
    }

    fn log_running(&self) {
        self.log("Task {} is running", Yellow);
    }

    fn log_finished(&self) {
        let size = termion::terminal_size();
        let newline = if let Ok((width, _)) = size {
            " ".repeat(width as usize - 1) + "\r"
        } else {
            String::from("\n")
        };

        eprint!("{newline}");
        self.log("Task {} is finished", color::Green);
    }

    fn log<C: Color + Copy>(&self, message: &str, color: C) {
        eprintln!(
            "{}>>> {} {}",
            Fg(White),
            message.replace(
                "{}",
                &format!(
                    "{}{}'{}'{}{}",
                    Fg(color),
                    Bold,
                    self.name,
                    style::Reset,
                    Fg(White),
                )
            ),
            Fg(color::Reset)
        )
    }
}
