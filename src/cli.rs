// SPDX-FileCopyrightText: 2022 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use clap::{arg, builder::PossibleValuesParser, command, Arg, ArgAction, Parser};
use termion::style::{self, Bold, Underline};
use thiserror::Error;

use crate::{
    config::project::{LoadProjectError, ProjectConfig},
    context::Context,
    targets::{TargetType, TaskRunError},
    throw_error,
};

#[derive(Parser)]
#[command(author, version, about)]
pub struct Args {
    /// Targets list.
    ///
    /// Every target can be a stage name or a task name.
    ///
    /// e.g. `test`, `build`, `check::format`
    targets: Vec<String>,
}

#[derive(Debug)]
pub enum Target {
    Task(String, String),
    Stage(String),
    Workflow(String),
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("workflow can only be used as the only target")]
    WorkflowNotOnlyTarget,
    #[error(transparent)]
    Run(#[from] TaskRunError),
    #[error("all targets must have the same type")]
    InconsistentTargetTypes,
}

pub fn run(cfg: Option<(ProjectConfig, Context)>) -> Result<(), Error> {
    let mut cmd = command!()
        .arg(arg!(-l --list "List available targets").exclusive(true))
        .arg({
            let mut arg = Arg::new("targets").action(ArgAction::Append);

            if let Some((project, _)) = &cfg {
                arg = arg.value_parser(PossibleValuesParser::new({
                    let workflows = project
                        .workflows
                        .iter()
                        .flat_map(|workflow| workflow.keys())
                        .map(|workflow| format!("@{workflow}"));
                    let stages = project.stages.keys().cloned();
                    let tasks = project.stages.iter().flat_map(|(stage, tasks)| {
                        tasks.iter().map(move |(task, _)| format!("{stage}:{task}"))
                    });

                    workflows.chain(stages).chain(tasks)
                }));
            }

            arg
        });

    let args = cmd.clone().get_matches();

    if let Some(targets) = args.get_many::<String>("targets") {
        let (project, ctx) = cfg
            .ok_or(LoadProjectError::NoRoot)
            .unwrap_or_else(throw_error);

        let targets: Vec<Target> = targets
            .into_iter()
            .map(|target| {
                if target.starts_with('@') {
                    Target::Workflow(target.chars().skip(1).collect())
                } else if let &[stage, task] = &target.split(':').collect::<Vec<&str>>()[..] {
                    Target::Task(stage.to_owned(), task.to_owned())
                } else {
                    Target::Stage(target.to_owned())
                }
            })
            .collect();

        let targets_type = match &targets[0] {
            Target::Workflow(_) => TargetType::Workflow,
            Target::Stage(_) => TargetType::Stage,
            Target::Task(_, _) => TargetType::Task,
        };

        match targets_type {
            TargetType::Workflow => {
                if targets.len() > 1 {
                    return Err(Error::WorkflowNotOnlyTarget);
                }

                if let Target::Workflow(workflow) = &targets[0] {
                    project.run_workflow(workflow, ctx)?;
                } else {
                    return Err(Error::InconsistentTargetTypes);
                }
            }
            TargetType::Stage => {
                for target in &targets {
                    if let Target::Stage(stage) = target {
                        project.run_stage(stage, ctx.clone())?;
                    } else {
                        return Err(Error::InconsistentTargetTypes);
                    }
                }
            }
            TargetType::Task => {
                for target in &targets {
                    if let Target::Task(stage, task) = target {
                        project.run_task(stage, task, ctx.clone())?;
                    }
                }
            }
        }
    } else if args.get_flag("list") {
        let (project, _) = cfg
            .ok_or(LoadProjectError::NoRoot)
            .unwrap_or_else(throw_error);
        println!(
            "{}{}Stages:{}\n{}",
            Underline,
            Bold,
            style::Reset,
            project
                .stages
                .into_iter()
                .map(|(name, tasks)| format!(
                    "  {}{}{}: {}",
                    Bold,
                    name,
                    style::Reset,
                    tasks
                        .into_iter()
                        .map(|(task_name, _)| task_name)
                        .collect::<Vec<_>>()
                        .join(", ")
                ))
                .collect::<Vec<_>>()
                .join("\n")
        );

        let workflows = project.workflows.unwrap_or_default();
        if !workflows.is_empty() {
            println!(
                "\n{}{}Workflows:{}\n{}",
                Underline,
                Bold,
                style::Reset,
                workflows
                    .into_iter()
                    .map(|(workflow, stages)| format!(
                        "  {}{}{}: {}",
                        Bold,
                        workflow,
                        style::Reset,
                        stages.join(", ")
                    ))
                    .collect::<Vec<_>>()
                    .join("\n")
            );
        }
    } else {
        cmd.print_help().expect("Failed to print help");
    }

    Ok(())
}
